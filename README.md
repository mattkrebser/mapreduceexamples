# README #

MapReduce Examples

### What is this repository for? ###

* This repository contains examples of simple MapReduce programs. They are written in Java, Pig latin, and there are Oozie Workflows.
* Some of the examples assume a specific Hadoop File System directory layout and may be hard to run. The programs in this repository are intended to be used as references only.
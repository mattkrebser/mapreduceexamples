package stubs;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class IndexMapper extends Mapper<Text, Text, Text, Text> {

  @Override
  public void map(Text key, Text value, Context context) throws IOException,
      InterruptedException {
	  FileSplit fileSplit = (FileSplit) context.getInputSplit();
	  Path path = fileSplit.getPath();
	  String fileName = path.getName();
    
	  String[] words = value.toString().split("\\W+");
	  
	  if (words == null || words.length == 0)
		  return;
	  
	  for (int i = 0; i < words.length; i++)
	  {
		  if ( i != 0)
		  {
			  context.write(new Text(words[i]), new Text(fileName + "@" + key.toString()));
		  }
	  }
  }
}
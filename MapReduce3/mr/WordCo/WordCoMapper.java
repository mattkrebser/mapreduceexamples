package stubs;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WordCoMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
    
     String[] words = value.toString().split("\\W+");
     
     if (words == null || words.length <= 1) return;
     
     for (int i = 0; i < words.length; i++)
     {
    	 if (i + 1 >= words.length) break;
    	 if (words[i].isEmpty() || words[i + 1].isEmpty()) continue;
    	 context.write(new Text(words[i] + ", " + words[i + 1]), new IntWritable(1));
     }
  }
}

-- load the data sets
orders = LOAD '/user/training/orders' AS (order_id:int,
             cust_id:int,
             order_dtm:chararray);

details = LOAD '/user/training/order_details' AS (order_id:int,
             prod_id:int);

products = LOAD '/user/training/products' AS (prod_id:int,
             brand:chararray,
             name:chararray,
             price:int,
             cost:int,
             shipping_wt:int);


orders_2012 = FILTER orders BY order_dtm matches '^2012.*$';

grouped_orders = GROUP orders BY cust_id;
count_orders = FOREACH grouped_orders GENERATE group, COUNT(orders) AS numorders;
orders_5 = FILTER count_orders BY numorders >= 5;

customerorders = JOIN orders_5 BY group, orders_2012 BY cust_id;

order_details = JOIN customerorders BY orders_2012::order_id, details BY order_id;

order_products = JOIN order_details BY details::prod_id, products BY prod_id;

prices = FOREACH order_products GENERATE orders_5::group AS cust_id, products::price AS price;

grouptotal = GROUP prices BY cust_id;
ctotal = FOREACH grouptotal GENERATE group, SUM(prices.price) AS total;

SPLIT ctotal INTO
platinum IF total >= 10000,
gold IF total >= 5000 AND total < 10000,
silver IF total >= 2500 AND total < 5000;

STORE platinum INTO '/user/training/platinum';
STORE gold INTO '/user/training/gold';
STORE silver INTO '/user/training/silver';



/*
 * TODO: Find all customers who had at least five orders
 *       during 2012. For each such customer, calculate 
 *       the total price of all products he or she ordered
 *       during that year. Split those customers into three
 *       new groups based on the total amount:
 *
 *        platinum: At least $10,000
 *        gold:     At least $5,000 but less than $10,000
 *        silver:   At least $2,500 but less than $5,000
 *
 *       Write the customer ID and total price into a new
 *       directory in HDFS. After you run the script, use
 *       'hadoop fs -getmerge' to create a local text file
 *       containing the data for each of these three groups.
 *       Finally, use the 'head' or 'tail' commands to check 
 *       a few records from your results, and then use the  
 *       'wc -l' command to count the number of customers in 
 *       each group.
 */

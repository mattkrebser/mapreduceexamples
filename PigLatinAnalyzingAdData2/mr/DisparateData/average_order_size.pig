orders = LOAD 'orders' AS (order_id:int,
             cust_id:int,
             order_dtm:chararray);

details = LOAD 'order_details' AS (order_id:int,
             prod_id:int);


-- TODO (A): Include only records from during the ad campaign
recent = FILTER orders BY order_dtm matches '^2013-0[2345]-.*$';


-- TODO (B): Find all the orders containing the advertised product
tablets = FILTER details BY prod_id == 1274348;


-- TODO (C): Get the details for each of those orders
contains_tablets = JOIN tablets BY order_id, recent BY order_id;
grouped_order = GROUP contains_tablets  BY tablets::order_id;


-- TODO (D): Count the number of items in each order
order_count = FOREACH grouped_order GENERATE group, COUNT(contains_tablets) AS num_products;
grouped = GROUP order_count ALL;

-- TODO (E): Calculate the average number of items in each order
average = FOREACH grouped GENERATE AVG(order_count.num_products);

-- Display the final result to the screen
DUMP average;

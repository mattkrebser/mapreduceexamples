-- Register DataFu and define an alias for the function
REGISTER '/usr/lib/pig/datafu-*.jar';
DEFINE DIST datafu.pig.geo.HaversineDistInMiles;


cust_locations = LOAD '/user/training/distribution/cust_locations/'
                   AS (zip:chararray,
                       lat:double,
                       lon:double);

warehouses = LOAD '/user/training/distribution/warehouses.tsv'
                   AS (zip:chararray,
                       lat:double,
                       lon:double);  
             


-- Create a record for every combination of customer and
-- proposed distribution center location.
allcombos = CROSS cust_locations, warehouses;

-- Calculate the distance from the customer to the warehouse
distances = FOREACH allcombos GENERATE warehouses::zip AS zip, DIST(cust_locations::lat, cust_locations::lon, warehouses::lat, warehouses::lon) AS distance;

-- Calculate the average distance for all customers to each warehouse
groupedzip = GROUP distances BY zip;
avgdistances = FOREACH groupedzip GENERATE group AS zip, AVG(distances.distance) as avgdistance;

-- Display the result to the screen
DUMP avgdistances;

-- Load only the ad_data1 and ad_data2 directories
data = LOAD '$input' AS (campaign_id:chararray,
             date:chararray, time:chararray,
             keyword:chararray, display_site:chararray,
             placement:chararray, was_clicked:int, cpc:int);

-- A: Group everything so we can call the aggregate function
datagrouped = GROUP data ALL;

-- B: Count the records 
total = FOREACH datagrouped GENERATE COUNT(data) * MAX(data.cpc);

-- C: Display the result to the screen
DUMP total;


-- Load only the ad_data1 and ad_data2 directories
data = LOAD '$input' AS (campaign_id:chararray,
             date:chararray, time:chararray,
             keyword:chararray, display_site:chararray,
             placement:chararray, was_clicked:int, cpc:int);

grouped = GROUP data BY display_site;

by_site = FOREACH grouped {
  -- Include only records where the ad was clicked
     clicked = FILTER data BY was_clicked == 1;
  -- count the number of records in this group
     nclicked = COUNT(clicked);
     total = COUNT(data);
     rate = nclicked*100.0/total;
     GENERATE group, rate AS rate;
}

-- sort the records in ascending order of clickthrough rate
datasorted = ORDER by_site BY rate ASC;

-- show just the first three

topthree = LIMIT datasorted 3;

DUMP topthree;



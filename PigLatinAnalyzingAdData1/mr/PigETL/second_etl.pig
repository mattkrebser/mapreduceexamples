data = LOAD '/user/training/ad_data2.txt' USING PigStorage(',') AS (campaign_id:chararray, date:chararray, time:chararray, display_site:chararray, placement:chararray, was_clicked:int, cpc:int, keyword:chararray);

datadistinct = DISTINCT data;

datasorted = FOREACH datadistinct 
{
GENERATE campaign_id, REPLACE(date, '-', '/'), time, TRIM(UPPER(keyword)), display_site, placement, was_clicked, cpc;
}

STORE datasorted INTO '/user/training/ad_output' USING PigStorage('	');

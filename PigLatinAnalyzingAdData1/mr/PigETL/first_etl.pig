data = LOAD '/user/training/ad_data1.txt' AS (keyword:chararray, campaign_id:chararray, date:chararray, time:chararray, display_site:chararray, was_clicked:int, cpc:int, country:chararray, placement:chararray);

dataus = FILTER data BY country == 'USA';

datasorted = FOREACH dataus 
{
GENERATE campaign_id, date, time, TRIM(UPPER(keyword)), display_site, placement, was_clicked, cpc;
}

STORE datasorted INTO '/user/training/ad_output1' USING PigStorage('	');

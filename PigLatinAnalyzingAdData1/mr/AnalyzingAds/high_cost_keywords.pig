-- TODO (A): Replace 'FIXME' to load the test_ad_data.txt file.

data = LOAD '$input' AS (campaign_id:chararray,
             date:chararray, time:chararray,
             keyword:chararray, display_site:chararray, 
             placement:chararray, was_clicked:int, cpc:int);


-- TODO (B): Include only records where was_clicked has a value of 1
dataclicked = FILTER data BY was_clicked == 1;

-- TODO (C): Group the data by the appropriate field
datagrouped = GROUP dataclicked BY keyword;

/* TODO (D): Create a new relation which includes only the 
 *           display site and the total cost of all clicks 
 *           on that site
 */

datacost = FOREACH datagrouped GENERATE group, SUM(dataclicked.cpc) AS sum;

-- TODO (E): Sort that new relation by cost (ascending)
datasorted = ORDER datacost BY sum DESC;


-- TODO (F): Display just the first three records to the screen
onlythree = LIMIT datasorted 5;

DUMP onlythree;

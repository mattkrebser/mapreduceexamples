package stubs;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;

import stubs.StringPairMapper;

public class StringPairTestDriver  {


  public static int main(String[] args) throws Exception {
	  
	   if (args.length != 2) {
	      System.out.printf("Usage: "  + " <input dir> <output dir>\n");
	      return -1;
	   }

	   Job job = new Job();
	   job.setJarByClass(StringPairTestDriver.class);
	   job.setJobName("Custom Writable Comparable");

	   FileInputFormat.setInputPaths(job, new Path(args[0]));
	   FileOutputFormat.setOutputPath(job, new Path(args[1]));
	      
	   job.setReducerClass(StringPairReducer.class);
	   job.setMapperClass(StringPairMapper.class);
	      
	   job.setOutputValueClass(LongWritable.class);
	   job.setOutputKeyClass(StringPairWritable.class);

	   boolean success = job.waitForCompletion(true);
	   return success ? 0 : 1;
  }
}

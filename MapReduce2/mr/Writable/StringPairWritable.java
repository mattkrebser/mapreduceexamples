package stubs;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.io.WritableComparable;

public class StringPairWritable implements WritableComparable<StringPairWritable> {

  Text left;
  Text right;

  /**
   * Empty constructor - required for serialization.
   */ 
  public StringPairWritable() {
	  left = new Text(); right = new Text();
  }

  /**
   * Constructor with two String objects provided as input.
   */ 
  public StringPairWritable(String left, String right) {
    this.left = new Text(left);
    this.right = new Text(right);    
  }

  /**
   * Serializes the fields of this object to out.
   */
  public void write(DataOutput out) throws IOException {
    
    /*
     * TODO implement
     */
    left.write(out);
    right.write(out);
  }

  /**
   * Deserializes the fields of this object from in.
   */
  public void readFields(DataInput in) throws IOException {
    
    /*
     * TODO implement
     */
    left.readFields(in);
    right.readFields(in);
  }

  /**
   * Compares this object to another StringPairWritable object by
   * comparing the left strings first. If the left strings are equal,
   * then the right strings are compared.
   */
  public int compareTo(StringPairWritable other) {
    if (other == null)
    	return -1;
    int val = 0;
    if ((val = left.compareTo(other.left)) == 0)
    {
    	return right.compareTo(other.right);
    }
    return val;
  }

  /**
   * A custom method that returns the two strings in the 
   * StringPairWritable object inside parentheses and separated by
   * a comma. For example: "(left,right)".
   */
  public String toString() {
     return "(" + left.toString() + ", " + right.toString() + ")";
  }

  @Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	
	if (obj == null)
		return false;
	
	if (getClass() != obj.getClass())
		return false;
	
	StringPairWritable other = (StringPairWritable) obj;
	
	if (left == null) {
		if (other.left != null)
			return false;
	} else if (!left.equals(other.left))
		return false;
	
	if (right == null) {
		if (other.right != null)
			return false;
	} else if (!right.equals(other.right))
		return false;
	
	return true;
}

  @Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((left == null) ? 0 : left.hashCode());
	result = prime * result + ((right == null) ? 0 : right.hashCode());
	return result;
}
}

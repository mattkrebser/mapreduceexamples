package stubs;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class StringPairReducer extends Reducer<StringPairWritable, LongWritable, StringPairWritable, LongWritable> {

  @Override
	public void reduce(StringPairWritable key, Iterable<LongWritable> values, Context context)
			throws IOException, InterruptedException {
		long wordCount = 0;

		for (LongWritable value : values) {
		  
			wordCount += value.get();
		}
		
		context.write(key, new LongWritable(wordCount));
	}
}
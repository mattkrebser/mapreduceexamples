package stubs;

import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogMonthMapper extends Mapper<LongWritable, Text, Text, Text> {

  /**
   * Example input line:
   * 96.7.4.14 - - [24/Apr/2011:04:20:11 -0400] "GET /cat.jpg HTTP/1.1" 200 12433
   *
   */
  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
    
	    String rgx = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
	    Pattern pattern = Pattern.compile(rgx);
	    Matcher matcher = pattern.matcher(value.toString());
	    
	    String val = value.toString();
	    int pos = val.indexOf("/");
	    String month = val.substring(pos + 1, pos + 4);
	    
	    HashMap<String, Integer> months = new HashMap<String, Integer>();
		  months.put("Jan", 0);
		  months.put("Feb", 1);
		  months.put("Mar", 2);
		  months.put("Apr", 3);
		  months.put("May", 4);
		  months.put("Jun", 5);
		  months.put("Jul", 6);
		  months.put("Aug", 7);
		  months.put("Sep", 8);
		  months.put("Oct", 9);
		  months.put("Nov", 10);
		  months.put("Dec", 11);

	    if (matcher.find() && months.containsKey(month))
	    {
	    	context.write(new Text(matcher.group(0)), new Text(month));
	    }
	  
  }
}

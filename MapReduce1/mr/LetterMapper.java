package stubs;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class LetterMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {

    for (String s : value.toString().split("\\W+"))
    {
    	if (s.length() > 0)
    	{
    		String first = ""; 
    		first += s.charAt(0);
    	
    		context.write(new Text(first.toString()), new IntWritable(s.length()));
    	}
    }
  }
}

package stubs;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class AverageReducer extends Reducer<Text, IntWritable, Text, DoubleWritable> {

  @Override
  public void reduce(Text key, Iterable<IntWritable> values, Context context)
      throws IOException, InterruptedException {

    double average = 0.0d;
    int sum = 0; int length = 0;
    
    for (IntWritable i : values)
    {
    	sum += i.get(); length++;
    }
    average = (double)sum / (double)length;
    
    context.write(key, new DoubleWritable(average));
  }
}
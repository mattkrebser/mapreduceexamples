package stubs;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.Mapper;

public class TFMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {

	  FileSplit fileSplit = (FileSplit) context.getInputSplit();
	  Path path = fileSplit.getPath();
	  String fileName = path.getName();
	  
	  Configuration c = context.getConfiguration();
	  
	  
	  for (String s : value.toString().split("\\W+"))
	  {
		  context.write(new Text(s + " " + fileName), new IntWritable((int)c.getLong("N", 0)));
	  }
  }
}

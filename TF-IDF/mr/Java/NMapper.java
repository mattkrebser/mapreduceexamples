package stubs;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;

import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;

public class NMapper extends Mapper<LongWritable, Text, Text, Text> {

  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
	  
	  String[] split = value.toString().split("\\W+");

	  if (split == null || split.length <= 3) return;

	  String fName = split[1];
	  String term = split[0];
	  String frequency = split[2];
	  String N = split[3];
	  
	  context.write(new Text(term), new Text(fName + " " + frequency + " " + N));
  }
}

package stubs;
import java.io.IOException;

import java.util.*;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;



public class NReducer extends Reducer<Text, Text, Text, Text> {

  @Override
  public void reduce(Text key, Iterable<Text> values, Context context)
      throws IOException, InterruptedException {

	    //using a new list because the provided iterable can only be iterated through one time
	    //ugh so lame
	    List<String> list = new ArrayList<String>(4);
	    //get docCount (number of documents that this word appears in)
	    int docCount = 0;
	    for (Text item : values) {list.add(item.toString()); docCount++; }
	    
	    if (docCount > 0)
	    {
		    //iterate through each key-value pair
	    	for (String t1 : list)
	    	{
	    		String[] split = t1.split("\\W+");
	    		if (split != null && split.length >= 3)
	    		{
	    			context.write(new Text(key.toString() + " " + split[0]),
	    					new Text(split[1] + " " +Integer.toString(docCount)+ " " + split[2]));
	    		}
	    	}
	    }
  }
}
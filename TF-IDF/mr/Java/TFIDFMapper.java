package stubs;
import java.io.IOException;
import java.lang.Math;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;

import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;

public class TFIDFMapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {

  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
	  
	  String[] split = value.toString().split("\\W+");

	  if (split == null || split.length <= 4) return;

	  String fName = split[1];
	  String term = split[0];
	  String frequency = split[2];
	  String docCount = split[3];
	  String NumDocs = split[4];
	  int n = Integer.parseInt(docCount);
	  double N = Double.parseDouble(NumDocs);
	  double IDF = Math.log10(N / (double)n);
	  
	  context.write(new Text(term + " " + fName),
			  new DoubleWritable(Integer.parseInt(frequency) * IDF));
  }
}

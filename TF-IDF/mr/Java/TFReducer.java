package stubs;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class TFReducer extends Reducer<Text, IntWritable, Text, Text> {

  @Override
  public void reduce(Text key, Iterable<IntWritable> values, Context context)
      throws IOException, InterruptedException {

	    int sum = 0;
	    int count = 0;
	    //each input value 'i' is equal to the number of documents (N)
	  	for (IntWritable i : values)
	  	{
	  		sum ++;
	  		int val = i.get();
	  		//assign count
	  		if (val > count) count = val;
	  	}
	  	context.write(key, new Text(Integer.toString(sum) + " " + Integer.toString(count)));
  }
}